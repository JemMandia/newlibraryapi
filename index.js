var mysql = require('mysql');
var express = require('express');
var cors = require('cors');
var app = express();

app.use(express.json());
app.use(cors());

var connection = mysql.createConnection ({
        host: 'localhost',
        user: 'root',
        password: '',
        database : 'library'
});

//Borrower Crud
app.get('/usersList', function(request, response){
    
    connection.query("SELECT  * FROM  users",  function(err, rows, fields)
    {      
        if(err){
            response.status(500).send();
        }else{
            result = Object.values(JSON.parse(JSON.stringify(rows)))
            response.status(200).send(result);
        }
    });
})

app.post('/user/add', function(request, response){
    user_fullname = request.body.user_fullname;
    user_address = request.body.user_address;
    connection.query(
        "insert into users (user_fullname,user_address) values ('"+user_fullname+"','"+user_address+"')", 
            function(err, rows, fields)
    {      
        if(err){
            response.status(500).send();
        }else{
            response.status(200).send();
        }
    });
})
app.put('/user/update/:id', function(request, response){
    const { id } = request.params;
    user_fullname = request.body.user_fullname;
    user_address = request.body.user_address;
    connection.query(
        `update users set user_fullname = '${user_fullname}',user_address = '${user_address}' WHERE user_id = '${id}'`, 
            function(err, rows, fields)
    {      
        if(err){
            response.status(500).send();
        }else{
            console.log('Updated Succesfully!');
            response.status(200).send();
        }
    });
})
app.delete('/user/delete/:id', function(request, response){
    
    const { id } = request.params;
    connection.query(
        `delete from users where user_id = '${id}'`, 
            function(err, rows, fields)
    {      
        if(err){
            response.status(500).send();
        }else{
            console.log('Deleted Succesfully!');
            response.status(200).send();
        }
    });
})

//Employee Crud
app.get('/employeeList', function(request, response){
    
    connection.query("SELECT  * FROM  employee",  function(err, rows, fields)
    {      
        if(err){
            response.status(500).send();
        }else{
            result = Object.values(JSON.parse(JSON.stringify(rows)))
            response.status(200).send(result);
        }
    });
})
app.post('/employee/add', function(request, response){
    employee_name = request.body.employee_name;
    connection.query(
        "insert into employee (employee_name) values ('"+employee_name+"')", 
            function(err, rows, fields)
    {      
        if(err){
            response.status(500).send();
        }else{
            response.status(200).send();
        }
    });
})
app.put('/employee/update/:id', function(request, response){
    const { id } = request.params;
    employee_name = request.body.employee_name;
    connection.query(
        `update employee set employee_name = '${employee_name}' WHERE employee_id = '${id}'`, 
            function(err, rows, fields)
    {      
        if(err){
            response.status(500).send();
        }else{
            console.log('Updated Succesfully!');
            response.status(200).send();
        }
    });
})
app.delete('/employee/delete/:id', function(request, response){
    
    const { id } = request.params;
    connection.query(
        `delete from employee where employee_id = '${id}'`, 
            function(err, rows, fields)
    {      
        if(err){
            response.status(500).send();
        }else{
            console.log('Deleted Succesfully!');
            response.status(200).send();
        }
    });
})

//Books Crud
app.get('/books/borrow', function(request, response){
    
    connection.query("SELECT  * FROM  books where stocks > 0",  function(err, rows, fields)
    {      
        if(err){
            response.status(500).send();
        }else{
            result = Object.values(JSON.parse(JSON.stringify(rows)))
            response.status(200).send(result);
        }
    });
})
app.get('/books', function(request, response){
    
    connection.query("SELECT  * FROM  books",  function(err, rows, fields)
    {      
        if(err){
            response.status(500).send();
        }else{
            result = Object.values(JSON.parse(JSON.stringify(rows)))
            response.status(200).send(result);
        }
    });
})
app.post('/books/add', function(request, response){

    book_name = request.body.book_name;
    book_author = request.body.book_author;
    stocks = request.body.stocks;
    
    connection.query(
        "insert into books (book_name,book_author,stocks) values ('"+book_name+"','"+book_author+"',"+stocks+")", 
            function(err, rows, fields)
    {      
        if(err){
            response.status(500).send();
        }else{
            response.status(200).send();
        }
    });
})
app.put('/books/update/:id', function(request, response){
    const { id } = request.params;
    book_name = request.body.book_name;
    book_author = request.body.book_author;
    stocks = request.body.stocks;
    connection.query(
        `update books set book_name = '${book_name}',book_author = '${book_author}',stocks = ${stocks} WHERE book_id = '${id}'`, 
            function(err, rows, fields)
    {      
        if(err){
            response.status(500).send();
        }else{
            console.log('Updated Succesfully!');
            response.status(200).send();
        }
    });
})
//update stock subtract
app.put('/books/update/stocksSub/:id', function(request, response){
    const { id } = request.params;
    connection.query(
        `update books set stocks = (stocks - 1) WHERE book_id = '${id}'`, 
            function(err, rows, fields)
    {      
        if(err){
            response.status(500).send();
        }else{
            console.log('Updated Succesfully!');
            response.status(200).send();
        }
    });
})
//update stock add
app.put('/books/update/stocksAdd/:id', function(request, response){
    const { id } = request.params;
    connection.query(
        `update books set stocks = (stocks + 1) WHERE book_id = '${id}'`, 
            function(err, rows, fields)
    {      
        if(err){
            response.status(500).send();
        }else{
            console.log('Updated Succesfully!');
            response.status(200).send();
        }
    });
})
//end of update stock
app.delete('/books/delete/:id', function(request, response){
    
    const { id } = request.params;
    connection.query(
        `delete from books where book_id = '${id}'`, 
            function(err, rows, fields)
    {      
        if(err){
            response.status(500).send();
        }else{
            console.log('Deleted Succesfully!');
            response.status(200).send();
        }
    });
})

//Transactions Crud
app.post('/logs/add', function(request, response){
    book_id = request.body.book_id;
    user_id = request.body.user_id;
    employee_id = request.body.employee_id;
    date_borrowed = request.body.date_borrowed;
    connection.query(
        "insert into logs (book_id,user_id,employee_id,date_borrowed) values ('"+book_id+"','"+user_id+"','"+employee_id+"','"+date_borrowed+"')", 
            function(err, rows, fields)
    {      
        if(err){
            response.status(500).send();
        }else{
            response.status(200).send();
        }
    });
})
app.delete('/deleteLogs/:id', function(request, response){
    
    const { id } = request.params;
    connection.query(
        `delete from logs where tran_id = '${id}'`, 
            function(err, rows, fields)
    {      
        if(err){
            response.status(500).send();
        }else{
            console.log('Deleted Succesfully!');
            response.status(200).send();
        }
    });
})
app.put('/updateLogs/:id', function(request, response){
    const { id } = request.params;
    date_returned = request.body.date_returned;
    console.log(date_returned)
    connection.query(
        `update logs set date_returned = '${date_returned}' WHERE tran_id = '${id}'`, 
            function(err, rows, fields)
    {      
        if(err){
            response.status(500).send();
        }else{
            console.log('Updated Succesfully!');
            response.status(200).send();
        }
    });
})
app.get('/logs', function(request, response){
    
    connection.query("SELECT a.tran_id,a.book_id,b.book_name,c.user_fullname,d.employee_name,DATE_FORMAT(a.date_borrowed, '%M %d %Y') as date_borrowed,IFNULL(DATE_FORMAT(a.date_returned, '%M %d %Y'),'Unreturned') as date_returned FROM logs a LEFT JOIN books b ON a.book_id = b.book_id LEFT JOIN users c ON a.user_id = c.user_id LEFT JOIN employee d ON a.employee_id = d.employee_id",  function(err, rows, fields)
    {      
        if(err){
            response.status(500).send();
        }else{
            result = Object.values(JSON.parse(JSON.stringify(rows)))
            response.status(200).send(result);
        }
    });
})

app.listen(4000);